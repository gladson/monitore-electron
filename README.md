# MonitoRE - Sistema de Monitoramento para Robótica Educacional

O Sistema de Monitoramento para Robótica Educacional - MonitoRE foi desenvolvido com o objetivo de apoiar as atividades envolvendo robótica móvel em Instituições de Ensino, contribuindo para aprimorar a experiência de professores e alunos em meio aos ambientes de tarefas da Robótica Educacional.
Dessa forma, os ambientes de tarefas foram classificados em três níveis de dificuldade. Nível 1 - apresenta o desafio dos marcadores, onde o robô móvel deve alcançar marcas coloridas no cenário; Nível 2 - apresenta o desafio de resgate, onde o robô móvel deve resgatar uma vítima e coloca-la em um lugar seguro; Nível 3 - apresenta o desafio da viagem ao centro da terra, onde o robô móvel devem "viajar" até encontrar um objeto valioso e trazê-lo de volta a "superfície".
Sendo assim, junte seus alunos e proponha os desafios! Os alunos deverão trabalhar em equipes (até 4 integrantes) para projetar e construir seus próprios robôs, além de planejar e executar uma estratégia para que o robô cumpra com os desafios de forma autônoma.
Então, mãos a obra!


**Clone and run for a quick way to see Electron in action.**

This is a minimal Electron application based on the [Quick Start Guide](http://electron.atom.io/docs/tutorial/quick-start) within the Electron documentation.

**Use this app along with the [Electron API Demos](http://electron.atom.io/#get-started) app for API code examples to help you get started.**

## To Use

To clone and run this repository you'll need [Git](https://git-scm.com) and [Node.js](https://nodejs.org/en/download/) (which comes with [npm](http://npmjs.com)) installed on your computer. From your command line:

```bash
# Manage components that contain HTML, CSS, JavaScript, fonts or even image files.
npm install -g bower
# Install all dependencies for a project
npm install -g yarn
# Clone this repository
git clone git@bitbucket.org:gladson/monitore-electron.git
# Go into the repository
cd monitore-electron
# Install dependencies
yarn install
# Run the app
yarn start
```

Learn more about Electron and its API in the [documentation](http://electron.atom.io/docs/).