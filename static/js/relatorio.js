const path = require('path');

var ourRequest = new XMLHttpRequest();
ourRequest.overrideMimeType("application/json");
ourRequest.open('GET', path.join(__dirname, '../../static/json/relatorio.json'), true);
ourRequest.onload = function() {
  if (ourRequest.status >= 200 && ourRequest.status < 400) {
    var data = JSON.parse(ourRequest.responseText);
    //console.log(data);
    createHTML(data);
  } else {
    console.log("We connected to the server, but it returned an error.");
  }
};

ourRequest.onerror = function() {
  console.log("Connection error");
};

ourRequest.send();

function createHTML(relatorioData) {
  var rawTemplate = document.getElementById("relatorioTemplate").innerHTML;
  var compiledTemplate = Handlebars.compile(rawTemplate);
  var ourGeneratedHTML = compiledTemplate(relatorioData);

  var relatorioTbody = document.getElementById("relatorio_list_json");
  relatorioTbody.innerHTML = ourGeneratedHTML;
}